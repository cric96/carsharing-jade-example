Car Sharing -- JADE iterative example
=====================================
by Giovanni Ciatto

Let's project and implement a distributed car sharing application helping wanna-be passengers to look for drivers and 
drivers to fill their cars thus making the journey cheaper for everybody!

Requirements
------------

There exists two kind of users: **drivers** and **passengers**.

Each driver is willing to travel from `startingPlace` to `destination` using his/her car, which has `nTotalSeats` seats. 
The driver wants to leave at `startingDateTime` and won't change his/her mind, no matter what.
The driver may already have invited some friends to join him/her, so `1  <= nReservedSeats < nTotalSeats` may 
be already reserved. 
The extimated cost of the journey is `totalCost` and will be divided between each person joining the driver, including 
the driver him-/her-self.
Because of their lazyness, drivers won't accept passegers wanting to leave from (resp. arrive to) a place which is not 
`startingPlace` (resp. `destination`).

Each passenger is looking for a passage from `startingPlace` to `destination` for him-/her-self and its 
`nNeededSeats - 1` friends which would allow them be in `destination` no later than `preferredArrivalDateTime`.
The passenger already knows that a fair quote for the travel he/she needs is `preferredQuote` and that the 
travel average duration is `expectedDuration`.
Thus the passenger is searching for a driver:
* having enough free seats into his/her car;
* leaving from a place close to `startingPlace`;
* arriving to a place close to `destination`;
* proposing a satisfying quota, given costs-over-seats ratio;
* letting him/her be in `destination` on time.

Both drivers and passengers will employ their smart-devices featured with a JADE-based application which will 
automatically take care of their business.

### Scenario

Drivers are lazy, so they will just wait for passengers making the first move, hoping to reduce their costs.
They will just leave when their deadline expires.

Conversely, passengers really need to find some driver to reach their destinations.
Thus they will actively search for drivers, selecting the very best offer.
When some passenger finds a good offer, he/her proposes a deal that could be accepted or refused by the driver.
If it is refused than nothing happens.
If it is accepted than both the driver and the passenger are **committed** to the deal, i.e., they won't change their 
mind.

Passengers are done once they are committed to a deal.

Drivers are done once they are committed to enough passengers to fill their car or their deadline has expired.

Exercise 0
----------
* Have a look to the project environment

* You will mainly work on classes `PassengerAgent` and `DriverAgent`, from package `it.unibo.ds.jade.carsharing`
    - They both extends `CarSharingAgent`, containing some shared methods you find useful
    - The provided implementation just initializes some input values and make agents greet each other
    - Try to understand how do they work: it will ease the next exercise

* Classes in `it.unibo.ds.jade.behaviors` are ready-to-use general purpose behaviors
    - They're **not** official JADE behaviors, but I think you will find them useful
    - Try to understand what's their purpose and functioning
    
* Classes in `it.unibo.ds.jade.messages` contains some utility methods which may be useful 
when manipulating `ACLMessages`

* In order to prevent wasting of time, you have been provided with a **Gradle** script facilitating you when building 
or running your exercises
    - try opening a shell within the directory containing this READMe and try running `gradle` or `gradlew`
    - ensure a `BUILD SUCCESSFUL` message is shown
    
* From now own you will just need to run
    - `gradlew build` to compile your project
    - `gradlew runPlatform` to run an empty platform on your local machine
    - `gradlew runPlatformWithAgents` to run a platform on your local machine containing the agents specified 
    within the `gradle.properties` file
    
* By running `gradlew eclipse` or `gradlew idea` you can automatically generate configuration files for your favourite 
IDE.
    - It is warmly suggested to use an IDE in order to ease the debugging process

> When testing your exercises, you can comment/uncomment/add/remove agents lines in `gradle.properties` 
 
Exercise 1
----------
Try to project and implement a solution satisfying the aforementioned requirements.
You may need to reason about which behaviors should be added to each agent, why, which actions should they perform
and in which order.
For this first exercise, you can assume passengers already know drivers names when they start.

0. Passengers simply send a `QUERY_IF` message to all the drivers they already know, asking if they can join the travel
    - Class `Messages.PassageAvailability` is an example of payload for such a communication

0. Drivers may answer by `INFORM`ing the querying passenger about his/her passage conditions
    - Class `Messages.PassageProposal` is an example of payload for such a communication

0. Passengers collect the `INFORM`ations received from the drivers and select the best one according 
to their own criteria

0. After selecting the best proposal, each passenger `PROPOSE`s to join the driver offering it
    - Again, class `Messages.PassageProposal` is an example of payload for such a communication
    
0. The driver receiving such a proposal may:
    * `ACCEPT` the proposal, thus increasing the amount of reserved seats
    * `REJECT` it, for instance, because he/she has already filled up his/her car in the meanwhile
    
0. Both passengers and drivers should `log` some good-bye message and terminate once their duty has been accomplished

> You are kindly invited to commit now:

    git add .
    git commit -m "finish ex 1"

Exercise 2
----------
Knowing the names of the service providers is often an unrealistic assumption in distributed systems and an example of 
tight coupling too.

We could leverage on the **Directory Facilitator** (DF) to let passenger dynamically discover which drivers are providing 
the car sharing service.

* Improve class `PassengerAgent` to make passengers discover drivers **on setup**, by means of the `DFService::search` 
method
    - Notice that both `PassengerAgent`s and `DriverAgent`s register to the DF service thanks to the 
    `CarSharingAgent::setup` method


Exercise 3
----------
You may have noticed that the `QUERY`-`INFORM` and `PROPOSE`-`ACCEPT_PROPOSAL`|`REJECT_PROPOSAL` seem like 
some already defined communication patterns.
Indeeds, you have implemented a simplified variant of the [FIPA-Query](http://www.fipa.org/specs/fipa00027/SC00027H.html)
and [FIPA-Propose](http://www.fipa.org/specs/fipa00036/SC00036H.html) interaction protocols.

* You should know re-implement the communication-related part of Exercise 1 using the [AchieveRE](http://jade.tilab.com/doc/api/jade/proto/AchieveREInitiator.html)
and [Propose](http://jade.tilab.com/doc/api/jade/proto/ProposeInitiator.html) behavior classes provided within the `jade.proto` package.
    - Notice that both protocols come with both an _Initiator_ and a _Receiver_ class, oe for each role in the protocol
    

Exercise 4
----------
Searching for drivers just once is not sufficient.
What if new drivers join the system **after** some passenger has executed its `setup` method (the one calling `DFService::search`,
according to Exercise 2)?

* Instead of leveraging on method `DFService::search`, passengers should `SUBSCRIBE` to the DF service
    - This will let them receive a message every time an agent description matching some template is
    register to (or unregistered from) the DF service
    - You may need to understand and use class [DFSubscriber](http://jade.tilab.com/doc/api/jade/domain/DFSubscriber.html)
    - You may need to understand and use method `CarSharingAgent::subscribeToDFForCounterparts`

* Fix the AcceptRE and Propose behavior accordingly
