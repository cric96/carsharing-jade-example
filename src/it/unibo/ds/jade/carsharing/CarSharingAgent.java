package it.unibo.ds.jade.carsharing;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.DFSubscriber;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.stream.Stream;

abstract class CarSharingAgent extends Agent {
    protected static final String SERVICE_NAME = "car sharing";
    protected static final String ROLE_DRIVER = "driver";
    protected static final String ROLE_PASSENGER = "passenger";

    private final String role;

    /**
     * Initializes the
     *
     * @param role
     */
    CarSharingAgent(String role) {
        super();
        this.role = role;
    }

    protected abstract void initialize(Object... args);

    @Override
    protected void setup() {
        initialize(getArguments());
        try {
            publishMyServiceDescription();
        } catch (FIPAException e) {
            throw new RuntimeException(e);
        }

        log("Hi! I'm %s, a car sharing %s", getName(), role);
    }

    @Override
    protected void takeDown() {
        try {
            revokeMyServiceDescription();
        } catch (FIPAException e) {
            throw new RuntimeException(e);
        }
    }

    private void publishMyServiceDescription() throws FIPAException {
        final DFAgentDescription myDescription = new DFAgentDescription();
        myDescription.setName(getAID());

        final ServiceDescription myServiceDescription = new ServiceDescription();
        myServiceDescription.setName(SERVICE_NAME);
        myServiceDescription.setType(role);
        myDescription.addServices(myServiceDescription);

        DFService.register(this, myDescription);
    }

    protected final void revokeMyServiceDescription() throws FIPAException {
        DFService.deregister(this);
    }


    protected final void log(String format, Object... args) {
        final String logMessage = String.format(
                "[%s] " + format,
                Stream.concat(Stream.of(this.getAID().getName()), Arrays.stream(args)).toArray()
        );
        System.out.println(logMessage);
    }

    protected final DFAgentDescription counterpartDescription(final String counterPartRole) {
        final DFAgentDescription userDescription = new DFAgentDescription();
        final ServiceDescription serviceDescription = new ServiceDescription();
        serviceDescription.setName(SERVICE_NAME);
        serviceDescription.setType(counterPartRole);
        userDescription.addServices(serviceDescription);

        return userDescription;
    }

    protected final SearchConstraints getAllServiciesConstraint() {
        final SearchConstraints constraints = new SearchConstraints();
        constraints.setMaxResults(-1L);

        return constraints;
    }

    protected final void subscribeToDFForCounterparts(final String counterPartRole) {
        addBehaviour(new DFSubscriber(this, counterpartDescription(counterPartRole)) {
            @Override
            public void onRegister(DFAgentDescription ad) {
                onCounterpartRegistered(ad.getName());
            }

            @Override
            public void onDeregister(DFAgentDescription ad) {
                onCounterpartDeregistered(ad.getName());
            }
        });


    }

    protected void onCounterpartRegistered(AID counterpartAID) {
        // TODO fill me in exercise 4
    }

    protected void onCounterpartDeregistered(AID counterpartAID) {
        // TODO fill me in exercise 4
    }

}
