package it.unibo.ds.jade.carsharing;

import java.util.Arrays;

public enum Places {
    MILANO,
    ROMA,
    BOLOGNA,
    FIRENZE,
    CESENA,
    RIMINI;

    public static Places fromName(final String name) {
        return Arrays.stream(Places.values())
                .filter(sp -> sp.name().equalsIgnoreCase(name))
                .findFirst()
                .get();
    }

    public boolean isCloseTo(final Places place) {
        return this == place;
    }
}
