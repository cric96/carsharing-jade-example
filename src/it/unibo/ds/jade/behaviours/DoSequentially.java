package it.unibo.ds.jade.behaviours;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;

import java.util.Arrays;

public class DoSequentially extends SequentialBehaviour {
    public DoSequentially(final Behaviour b, final Behaviour... bs) {
        super();
        this.addSubBehaviours(b, bs);
    }

    public DoSequentially(final Agent agent, final Behaviour b, final Behaviour... bs) {
        super(agent);
        this.addSubBehaviours(b, bs);
    }

    private void addSubBehaviours(Behaviour b, Behaviour... bs) {
        this.addSubBehaviour(b);
        Arrays.stream(bs).forEach(this::addSubBehaviour);
    }
}
