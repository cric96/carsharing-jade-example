package it.unibo.ds.jade.behaviours;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public abstract class WaitForMessage extends SimpleBehaviour {

    private final MessageTemplate template;
    private ACLMessage received;

    public WaitForMessage(MessageTemplate template) {
        this.template = template;
    }

    public WaitForMessage(Agent a, MessageTemplate template) {
        super(a);
        this.template = template;
    }

    @Override
    public void action() {
        received = getAgent().receive(template);
        if (received != null) {
            handleMessage(received, template);
        } else {
            this.block();
        }
    }

    protected abstract void handleMessage(final ACLMessage receivedMessage, final MessageTemplate template);

    @Override
    public boolean done() {
        return received != null;
    }
}
