package it.unibo.ds.jade.behaviours;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.SequentialBehaviour;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

public class DoAllConcurrently extends ParallelBehaviour {
    public DoAllConcurrently(final Behaviour b, final Behaviour... bs) {
        this(null, b, bs);
    }

    public DoAllConcurrently(final Stream<Behaviour> bs) {
        this(null, bs);
    }

    public DoAllConcurrently(final Agent agent, final Behaviour b, final Behaviour... bs) {
        super(agent, ParallelBehaviour.WHEN_ALL);
        this.addSubBehaviour(b);
        this.addSubBehaviours(Arrays.stream(bs));
    }

    public DoAllConcurrently(final Agent agent, final Stream<Behaviour> bs) {
        super(agent, ParallelBehaviour.WHEN_ALL);
        this.addSubBehaviours(bs);
    }

    private void addSubBehaviours(Stream<Behaviour> bs) {
        bs.forEach(this::addSubBehaviour);
    }
}
